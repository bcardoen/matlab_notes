### Matlab Notes

##### Multiple variable assignment (arg unpacking)
```Matlab
[x y z] = deal(1, 2, 3)
[x y z] = deal(1)
```
